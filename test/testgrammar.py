#!/usr/bin/env python

import argparse
import json

from novoapi.backend import session

p = argparse.ArgumentParser()
p.add_argument("--user", default=None)
p.add_argument("--password", default=None)
args = p.parse_args()

rec = session.Recognizer(grammar_version="1.0", lang="nl", snodeid=101, user=args.user, password=args.password, keepopen=True) # , modeldir=modeldir)

grammar = {
  "type": "confusion_network",
  "version": "1.0",
  "data": {
    "kind": "sequence",
    "elements": [
      {
        "kind": "word",
        "pronunciation": [
          {
            "phones": [
              "wv",
              "a1",
              "n"
            ],
            "id": 0
          },
          {
            "phones": [
              "wv",
              "uh1",
              "n"
            ],
            "id": 1
          }
        ],
        "label": "one"
      },
      {
        "kind": "word",
        "pronunciation": [
          {
            "phones": [
              "t",
              "uw1"
            ],
            "id": 0
          }
        ],
        "label": "two"
      },
      {
        "kind": "word",
        "pronunciation": [
          {
            "phones": [
              "t",
              "r",
              "iy1"
            ],
            "id": 0
          },
          {
            "phones": [
              "s",
              "r",
              "iy1"
            ],
            "id": 1
          }
        ],
        "label": "three"
      }
    ]
  },
  "return_objects": [
    "grammar"
  ],
  "phoneset": "novo70"
}

res = rec.setgrammar(grammar)
print "Set grammar result", res

res = rec.recognize_wav("test/onetwothree.wav")
print "Recognition result:", json.dumps(res.export(), indent=4)



