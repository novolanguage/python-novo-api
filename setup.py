#!/usr/bin/env python
# (c) 2015--2018 NovoLanguage, author: David A. van Leeuwen


from setuptools import setup

from novoapi import __version__

setup(name="novoapi",
      version=__version__,
      description="Novolanguage api",
      author="David A. van Leeuwen",
      author_email="david.vanleeuwen@gmail.com",
      license="Proprietary",
      classifiers=["Programming Language :: Python :: 2.7"],
      ## packages defines which part of the archive is actually copied
      packages=["novoapi", "novoapi.backend", "novoapi.asr", "novoapi.asr.segments", "novoapi.asr.spraaklab",
                "novoapi.utils.json"],
      scripts=["bin/forced-align.py"],
      install_requires=["requests", "jsonschema", "wave", "websocket-client"],
      dependency_links=[],
      package_data={"novoapi": ["test/wavscp", "test/text", "test/onetwothree.wav"]},
      include_package_data=True
)
