# Novo api

This is a small python2 package for accessing the novolanguage speech recognition API.  It is intended for research purposes only, and only for associates of Novolanguage. 

## What this package pretends to do

With this package, you should be able to connect to the automatic speech recognition services of Novolanguage, and carry out forced alignments to speech recordings.

## Install 

You should be able to install this package using `pip`, 
```bash
pip install --upgrade git+https://david_van_leeuwen@bitbucket.org/novolanguage/python-novo-api.git
```

## Forced alignment

The script for forced alignment should be installed by pip under something like `/usr/local/bin/forced-align.py`.  This script takes two files in Kaldi format: a `wavscp` file, containing a list of audio files, and a `text` file, containing the corresponding transcriptions. 

```bash
forced-align.py --user <username> --password <password> --lang <language> --format json <wavscp> <text>
```

The `<user>` and `<password>` are the credentials that you should have been given.  These are personal, do not share these.

The `<lang>` is a language code, `en` or `nl`.  
 
The `<wavscp>` file is a simple text file with lines of the format:
```bash
<id> <path/to/file.wav>
```
where `<id>` is just an identifier to identify the audio file. 

The `<text>` file is a simple text file containing the transcriptions corresponding to the audio files.  The format for each line is:

```bash
<id> <word1> <word2> ... <wordN>
```

### Example
If you download the files `wavscp`, `text` and `testonetwothree.wav` from the repository and place them in a directory `./test/`, you should be able to run:

```bash
forced-align.py --user username --password password --lang en --format json test/wavscp test/text | tee output.json
```

The output json file is a JSON file with the structure:
```json
[
  {
    "begin": 0,
    "end": 57,
    "score": 95.114,
    "phones": [
      {
        "begin": 0,
        "end": 57,
        "score": 95.114,
        "label": "sil",
        "llh": 1.234,
        "type": "phone"
      }
    ],
    "label": "<s>",
    "llh": 1.234,
    "type": "word"
  },
  {
    "begin": 57,
    "end": 84,
    "score": 97.402,
    "phones": [
      {
        "begin": 57,
        "end": 68,
        "score": 97.402,
        "label": "w",
        "llh": 1.89,
        "type": "phone"
      },
      {
        "begin": 68,
        "end": 74,
        "score": 98.675,
        "label": "ah1",
        "llh": 2.576,
        "type": "phone"
      },
      {
        "begin": 74,
        "end": 84,
        "score": 98.15,
        "label": "n",
        "llh": 2.237,
        "type": "phone"
      }
    ],
    "label": "one",
    "llh": 6.702,
    "type": "word"
  },
  ...
]
```
Here, begin/end times are in frames (10ms), labels are word or phone labels, score is an acoustic score presented as a value from 0--100, and llh is a log-likelihood acoustic score that is somewhat more fundamental.  

