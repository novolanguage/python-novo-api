#!/usr/bin/env python
# (c) 2015--2018 NovoLanguage, author: David A. van Leeuwen

## force-align audio using a backend

import argparse
import logging
import collections
import codecs
import json

from novoapi.backend import session

p = argparse.ArgumentParser()
p.add_argument("--lang", default="en")
p.add_argument("--format", default="segmentation")
p.add_argument("--snode", default=101, type=int)
p.add_argument("--user", default=None)
p.add_argument("--password", default=None)
p.add_argument("--debug", action="store_true")
p.add_argument("--showgrammar", action="store_true")
p.add_argument("wavscp")
p.add_argument("text")
args = p.parse_args()

if args.debug:
    logging.basicConfig(level=logging.DEBUG)

rec = session.Recognizer(grammar_version="1.0", lang=args.lang, snodeid=args.snode, user=args.user, password=args.password, keepopen=True) # , modeldir=modeldir)

def readscp(file):
    d = collections.OrderedDict()
    for line in codecs.open(file, encoding="utf-8"):
        id, value = line.strip().split(" ", 1)
        d[id] = value
    return d

wavscp = readscp(args.wavscp)
text = readscp(args.text)

for id in wavscp:
    wav = wavscp[id]
    words = text[id].split()
    ret = rec.set_alternatives_grammar([[w] for w in words], version="1.0", ret=["grammar"])
    ## alternative: rec.setgrammar(grammar) with grammar as printed below:
    if args.showgrammar:
        print json.dumps(ret["result"]["grammar"])
        continue
    if wav.startswith("http"):
        res = rec.recognize_url(wav)
    else:
        res = rec.recognize_wav(wav)
    if args.format == "segmentation":
        print res
    elif args.format == "json":
        print json.dumps(res.export())
    elif args.format == "backend":
        print rec.last_message

