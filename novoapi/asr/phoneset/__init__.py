#!/usr/bin/env python

import os
import glob
import codecs
import collections

phonesets = dict()
phoneset_bylang = collections.defaultdict(list)
fillers = ["<s>", "<sil>", "</s>", "!sil"]

for lang in ["en", "nl", "zh"]:
    for path in glob.glob(os.path.join(os.path.dirname(__file__), lang, "*.phoneset")):
        name, _ = os.path.splitext(os.path.basename(path))
        phones = dict()
        for line in codecs.open(path, encoding="utf8"):
            if line.startswith("#"):
                continue
            words = line.strip().split()
            if lang == "zh":
                ascii, ipa, ipaa, example = words[0:4]
                phones[ascii] = {"ipa": ipa, "ipaa": ipaa, "example": example, "translation": " ".join(words[4:])}
            else:
                ascii, ipa, example = words[0:3]
                phones[ascii] = {"ipa": ipa, "example": example}
        phonesets[name] = phones
        phoneset_bylang[lang].append(name)
