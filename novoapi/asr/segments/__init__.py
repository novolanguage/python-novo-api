#!/usr/bin/env python

from .segments import Segmentation
from .praat import seg2tg
